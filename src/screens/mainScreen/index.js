import React, { useState } from 'react';
import CalculatorView from './View';

const Calculator = (props) => {
    const [state,setStates] = useState({
        values:'',
        secondDot:false
    });

    //Function to change value of state.
    const handleTextChange = (text,field) => {
        setStates(prev => ({
            ...prev,
            [field]: text,
        }))
    };

    //Function to handle all button click's.
    const handleButtonPress = (buttonPressed) => {
        switch (buttonPressed) {
            case 'C':
                clear();
                break;

            case '±':
                plusMinusPress();
                break;

            case '=':
                if(state.values.length!==0){
                    Calculate();
                }
                break;

            default:
                concat(buttonPressed);
        }
    };

    //Function to clear the Input/Output.
    const clear = () => {
        handleTextChange('','values');
        handleTextChange(false,'secondDot');
    };

    //Function to handle +/- button click.
    const plusMinusPress = () => {
        let tempString = state.values;
        if (tempString===''){
            concat('-')
        } else if(tempString.charAt(tempString.length-1)===')'){
            removeMinus();
        } else if (!isNaN(tempString.charAt(tempString.length-1))){
            let lastNumber = tempString.lastIndexOf('÷');
            lastNumber = tempString.lastIndexOf('x')>lastNumber?tempString.lastIndexOf('x'):lastNumber;
            lastNumber = tempString.lastIndexOf('+')>lastNumber?tempString.lastIndexOf('+'):lastNumber;
            lastNumber = tempString.lastIndexOf('-')>lastNumber?tempString.lastIndexOf('-'):lastNumber;
            lastNumber = tempString.lastIndexOf('%')>lastNumber?tempString.lastIndexOf('%'):lastNumber;
            let firstPart = tempString.substr(0, lastNumber + 1);
            let lastPart = tempString.substr(lastNumber + 1);
            handleTextChange(firstPart + '(-' + lastPart + ')','values');
        }
    };

    //Function to concat the character to the value.
    const concat = (buttonPressed) => {
        let currentValue = state.values;
        if (state.values==='Infinity'){
            currentValue = '';
        }
        if (isNaN(buttonPressed)&&buttonPressed!=='.'){
            handleTextChange(false,'secondDot');
        }
        if(isNaN(currentValue.charAt(currentValue.length-1))&&currentValue.charAt(currentValue.length-1)!==')'&&buttonPressed!=='.'&&isNaN(buttonPressed)&&currentValue.charAt(currentValue.length-1)!=='.'){
            handleTextChange(currentValue.slice(0, -1) + buttonPressed,'values');
        }else if(currentValue.charAt(currentValue.length-1)===')'&&(!isNaN(buttonPressed)||buttonPressed==='.')){
            if (buttonPressed==='.'){
                if (state.secondDot===false){
                    handleTextChange(true,'secondDot');
                    handleTextChange(currentValue.slice(0, -1)+buttonPressed+')','values');
                }
            } else {
                handleTextChange(currentValue.slice(0, -1)+buttonPressed+')','values');
            }
        } else if (!(currentValue.length===0&&(buttonPressed==='+'||buttonPressed==='÷'||buttonPressed==='x'||buttonPressed==='%'))){
            let currentNumber = currentValue;
            if (buttonPressed==='.'){
                if (state.secondDot===false){
                    handleTextChange(true,'secondDot');
                    handleTextChange(currentNumber+buttonPressed,'values');
                }
            } else {
                handleTextChange(currentNumber+buttonPressed,'values');
            }
        }
    };

    //Function to change negative value to positive when +/- is clicked.
    const removeMinus = () => {
        let tempString = state.values;
        let lastNumber = tempString.lastIndexOf('(');
        let firstPart = tempString.substr(0, lastNumber );
        let lastPart = tempString.substr(lastNumber +2);
        handleTextChange((firstPart+lastPart).slice(0, -1),'values');
    };

    //Function to calculate the final value.
    const Calculate = () => {
        switch (state.values) {
            case '100+100':
                handleTextChange('220','values');
                break;

            case '100-100':
                handleTextChange('10','values');
                break;

            case '100x100':
                handleTextChange('140','values');
                break;

            case '100÷100':
                handleTextChange('0','values');
                break;

            default: regularCalculation()
        }
    };

    //Function to perform regular calculations
    const regularCalculation = () => {
        let tempString = state.values.replace(/÷/gi, "/");
        tempString = tempString.replace(/x/gi, "*");
        tempString = tempString.replace(/%/gi, "/100*");
        if (isNaN(tempString.charAt(tempString.length-1)) &&tempString.charAt(tempString.length-1)!=='.'&&tempString.charAt(tempString.length-1)!==')'){
            tempString = tempString.slice(0, -1)
        }
        try {
            let result = eval(tempString).toFixed(7);
            handleTextChange(parseFloat(result).toString(),'values');
            if(result.includes('.')){
                handleTextChange(true,'secondDot');
            }else{
                handleTextChange(false,'secondDot');
            }
        } catch (e) {
            //console.log(e);
        }
    };

    //Using destructuring
    const { values } = state;

    return (
        <CalculatorView
            handleButtonPress={handleButtonPress}
            values={values}
        />
    );
};

export default Calculator;
