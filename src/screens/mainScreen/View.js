import React from 'react';
import {View,Text} from 'react-native';
import Button from '../../component/Button';
import styles from './Style';

const CalculatorView = (props) => {

    const { values,handleButtonPress } = props;

    return(
        <View style={styles.mainContainer}>
            <View style={styles.resultContainer}>
                <Text style={styles.resultText}>
                    {values}
                </Text>
            </View>
            <View style={styles.numberContainer}>
                <View style={styles.buttonContainer}>
                    <Button color={'#bab9c1'} text={'C'} onClick={()=>handleButtonPress('C')}/>
                    <Button color={'#bab9c1'} text={'±'} onClick={()=>handleButtonPress('±')}/>
                    <Button color={'#bab9c1'} text={'%'} onClick={()=>handleButtonPress('%')}/>
                    <Button color={'#EF9B43'} text={'÷'} textColor={'#fff'} onClick={()=>handleButtonPress('÷')}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button text={'7'} onClick={()=>handleButtonPress('7')}/>
                    <Button text={'8'} onClick={()=>handleButtonPress('8')}/>
                    <Button text={'9'} onClick={()=>handleButtonPress('9')}/>
                    <Button color={'#EC963F'} text={'X'} textColor={'#fff'} onClick={()=>handleButtonPress('x')}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button text={'4'} onClick={()=>handleButtonPress('4')}/>
                    <Button text={'5'} onClick={()=>handleButtonPress('5')}/>
                    <Button text={'6'} onClick={()=>handleButtonPress('6')}/>
                    <Button color={'#EC923C'} text={'-'} textColor={'#fff'} onClick={()=>handleButtonPress('-')}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button text={'1'} onClick={()=>handleButtonPress('1')}/>
                    <Button text={'2'} onClick={()=>handleButtonPress('2')}/>
                    <Button text={'3'} onClick={()=>handleButtonPress('3')}/>
                    <Button color={'#E98C3F'} text={'+'} textColor={'#fff'} onClick={()=>handleButtonPress('+')}/>
                </View>
                <View style={styles.buttonContainer}>
                    <Button text={'0'} width={'50%'} onClick={()=>handleButtonPress('0')}/>
                    <Button text={'.'} onClick={()=>handleButtonPress('.')}/>
                    <Button color={'#E8873A'} text={'='} textColor={'#fff'} onClick={()=>handleButtonPress('=')}/>
                </View>
            </View>
        </View>
    )
};

export default CalculatorView;
