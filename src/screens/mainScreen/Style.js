import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1
    },
    resultContainer: {
        flex:1,
        backgroundColor:'#181619',
        justifyContent:'center'
    },
    resultText: {
        color:'#fff',
        fontSize:50,
        alignSelf:'flex-end'
    },
    numberContainer: {
        flex:3
    },
    buttonContainer: {
        flexDirection:'row',
        flex:1
    }
});

export default styles;
