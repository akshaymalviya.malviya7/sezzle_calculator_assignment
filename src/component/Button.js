import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import styles from './Style';
import PropTypes from 'prop-types';

const Button = (props) => {
    return(
        <TouchableOpacity
            onPress={props.onClick}
            style={[styles.button,{width:props.width,backgroundColor:props.color}]}>
            <Text style={[styles.text,{color:props.textColor}]}>{props.text}</Text>
        </TouchableOpacity>
    )
};

Button.propTypes = {
    text: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    color: PropTypes.string,
    width: PropTypes.string,
    textColor: PropTypes.string
};

Button.defaultProps = {
    color: '#E1E0E6',
    width: '25%',
    textColor: '#414046'
};

export default Button;
