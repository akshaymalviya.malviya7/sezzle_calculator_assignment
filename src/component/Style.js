import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    button: {
        paddingLeft:'10%',
        borderWidth:1,
        borderColor:'#919096',
        justifyContent:'center',
    },
    text: {
        fontSize:30
    },
});

export default styles;
